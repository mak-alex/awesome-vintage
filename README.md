
Vintage
=======

Vintage theme for Awesome WM 3.4.x

Autocheck connection to widget: br0, wlp3s0, enp3s0, wlan0, eth0

Binary clock

git clone --recursive git@bitbucket.org:d_e_n_i_e_d/awesome-vintage.git
![Awesome Vintage](https://raw.github.com/romockee/vintage/master/screenshots/vintage1.png)


![urxvt, weechat, tmux, ncmpcpp](https://raw.github.com/romockee/vintage/master/screenshots/vintage2.png)
